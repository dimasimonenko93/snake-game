﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace SnakeEngine
{
    public interface ISnake
    {
        event Action StepOnBody;
        event Action StepOnWall;
        event Action MovementDone;
        Position HeadPosition  { get; }
        Snake.Direction SnakeDirection { get; }
        List<Position> BodyPosition { get; }
        void GrowBody();
        void StartMove();
        void StopMove();
        void Reset();
    }
}
