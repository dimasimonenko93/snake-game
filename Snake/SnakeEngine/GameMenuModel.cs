﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeEngine
{
    public class GameMenuModel
    {
        public enum MenuId
        {
            continue_game,
            new_game,
            exit_game
        }

        public Dictionary<MenuId, GameMenuItem> MenuItems = new Dictionary<MenuId, GameMenuItem>();

        public GameMenuModel() 
        {
            MenuItems.Add(MenuId.continue_game, new GameMenuItem("Continue"));
            MenuItems.Add(MenuId.new_game, new GameMenuItem("New Game"));
            MenuItems.Add(MenuId.exit_game, new GameMenuItem("Exit"));
        }
    }
}