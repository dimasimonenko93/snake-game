﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeEngine
{
    public interface IBoardConfig
    {
        int Width  { get; }
        int Height { get; }
        int Frame  { get; }
    }
}
