﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace SnakeEngine
{
    public class Board : IBoard
    {
        public enum CellStatus { Empty, SnakeHead, SnakeBody, Rabbit, Wall };

        public event Action<Position> Changed = (p) => { };

        public int Width  { get; } 
        public int Height { get; } 
        public int Frame  { get; }

        public int Top    { get { return 0 + Frame; } }
        public int Bottom { get { return Height - Frame - 1; } }
        public int Right  { get { return Width - Frame - 1; } }
        public int Left   { get { return 0 + Frame; } }

        private CellStatus[,] array;

        public CellStatus this[int i, int j]
        {
            get
            {
                return array[i, j];
            }
            set
            {
                if (value != array[i, j])
                {
                    array[i, j] = value;
                    Changed(new Position(j, i));
                }
            }
        }

        public Board(IBoardConfig config)
        {
            Width  = config.Width;
            Height = config.Height;
            Frame  = config.Frame;
            array  = new CellStatus[Height, Width];
            CreateWall(); 
        }

        public void Clear()
        {
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    array[i, j] = CellStatus.Empty;
                }
            }
            CreateWall();
        }

        private void CreateWall()
        {
            for (int i = 0; i < Frame; i++) 
            {
                for (int j = 0; j < Width; j++)
                {
                    array[i, j] = CellStatus.Wall;                //Top frame
                    array[Height - i - 1, j] = CellStatus.Wall;   // Bottom frame
                }
                for (int j = 0; j < Height; j++)
                {
                    array[j, i] = CellStatus.Wall;                // Left frame
                    array[j, Width - i - 1] = CellStatus.Wall;    // Right frame
                }
            }
        }
    }
}