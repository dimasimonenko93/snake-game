﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeEngine
{
    public interface IGame
    {
        GameMenuModel GameMenu { get; }
        event Action GameOver;
        event Action GamePaused;
        event Action GameStarted;
        event Action GameStoped;
        void Start();
    }
}
