﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeEngine
{
    public sealed class GameMenuItem
    {
        public event Action Action;

        public string Text { get; private set; }

        public GameMenuItem(string Text)
        {
            this.Text = Text;
        }

        public void CallEvent()
        {
            Action();
        }
    }
}
