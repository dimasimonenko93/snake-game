﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeEngine
{
    public class Position
    {
        public int X;
        public int Y;

        public Position()
        {

        }

        public Position(int X, int Y)
        {
            this.X = X;
            this.Y = Y;
        }
    }
}
