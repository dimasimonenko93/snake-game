﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeEngine
{
    public interface IBoard
    {
        event Action<Position> Changed;
        int Width   { get; }
        int Height  { get; }
        int Frame   { get; }
        int Top     { get; }
        int Bottom  { get; }
        int Right   { get; }
        int Left    { get; }
        Board.CellStatus this[int i, int j] { get; set; }
        void Clear();
    }
}
