﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeEngine
{
    public abstract class GameController : IGameController
    {
        public enum KeyCode
        {
            VoidCode,
            EscapeCode,
            RightArrowCode,
            LeftArrowCode,
            UpArrowCode,
            DownArrowCode
        }

        public event Action ShowMenu;
        public event Action MoveRight;
        public event Action MoveLeft;
        public event Action MoveDown;
        public event Action MoveUp;

        private KeyCode PrevckiCode;

        protected void ReadKey(KeyCode Cki)
        {
            KeyCode CkiCode = Cki;

                if (CkiCode == KeyCode.RightArrowCode && PrevckiCode != KeyCode.LeftArrowCode)
                {
                    MoveRight();
                }
                else if (CkiCode == KeyCode.LeftArrowCode && PrevckiCode != KeyCode.RightArrowCode)
                {
                    MoveLeft();
                }
                else if (CkiCode == KeyCode.UpArrowCode && PrevckiCode != KeyCode.DownArrowCode)
                {
                    MoveUp();
                }
                else if (CkiCode == KeyCode.DownArrowCode && PrevckiCode != KeyCode.UpArrowCode)
                {
                    MoveDown();
                }
                else if (CkiCode == KeyCode.EscapeCode)
                {
                    ShowMenu();
                    CkiCode = PrevckiCode;
                }
                else
                {
                    CkiCode = PrevckiCode;
                }

                PrevckiCode = CkiCode;
        }

        public virtual void Start()
        {
        }

        public virtual void Stop()
        {
        }
    }
}
