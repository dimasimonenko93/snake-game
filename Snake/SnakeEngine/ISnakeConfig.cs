﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeEngine
{
    public interface ISnakeConfig
    {
        int SpeedOfSnake { get; }
    }
}
