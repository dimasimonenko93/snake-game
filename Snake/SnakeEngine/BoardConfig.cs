﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeEngine
{
    public class BoardConfig : IBoardConfig
    {
        public int Width  { get; } = Convert.ToInt32(ConfigurationManager.AppSettings["BoardWidth"]);
        public int Height { get; } = Convert.ToInt32(ConfigurationManager.AppSettings["BoardHeight"]);
        public int Frame  { get; } = Convert.ToInt32(ConfigurationManager.AppSettings["BoardFrame"]);
    }
}
