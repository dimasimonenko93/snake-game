﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace SnakeEngine
{
    public class Game : IGame
    {
        IBoard brd;
        ISnake snake;
        IRabbit rabbit;
        IGameController gameController;
        public GameMenuModel GameMenu { get; private set; }

        public event Action GameOver;
        public event Action GamePaused  = () => { };
        public event Action GameStarted = () => { };
        public event Action GameStoped  = () => { };

        public Game(IBoard brd, ISnake snake, IRabbit rabbit, IGameController gameController)
        {
            this.brd             = brd;
            this.snake           = snake;
            this.rabbit          = rabbit;
            this.gameController = gameController;

            snake.StepOnBody     += FinishTheGame;
            snake.StepOnWall     += FinishTheGame;
            gameController.ShowMenu += Pause;
            snake.MovementDone   += Step;

            GameMenu = new GameMenuModel();
            GameMenu.MenuItems[GameMenuModel.MenuId.continue_game].Action += Start;
            GameMenu.MenuItems[GameMenuModel.MenuId.new_game].Action      += RestartTheGame;
            Step();
        }

        public void Start()
        {
            gameController.Start();
            snake.StartMove();
            GameStarted();
        }

        private void Pause()
        {
            gameController.Stop();
            snake.StopMove();
            GamePaused();
        }

        private void Stop()
        {
            gameController.Stop();
            snake.StopMove();
            GameStoped();
        }

        private void Step()
        {
            if (IsSnakeHeadOnReabbit())
            {
                snake.GrowBody();
                rabbit.Count++;

                do
                {
                    rabbit.Create();
                }
                while (IsSnakeHeadOnReabbit() || IsRabbitOnSnakeBody());
            }
        }

        private bool IsSnakeHeadOnReabbit()
        {
            if (snake.HeadPosition.X == rabbit.position.X &&
                snake.HeadPosition.Y == rabbit.position.Y)
            {
                brd[rabbit.position.Y, rabbit.position.X] = Board.CellStatus.SnakeHead;
                return true;
            }
            else return false;
        }

        private bool IsRabbitOnSnakeBody()
        {
            foreach (Position position in snake.BodyPosition)
            {
                if (rabbit.position.X == position.X &&
                    rabbit.position.Y == position.Y)
                {
                    brd[rabbit.position.Y, rabbit.position.X] = Board.CellStatus.SnakeBody;
                    return true;
                }
            }
            return false;
        }

        private void FinishTheGame()
        {
            Stop();
            GameMenu.MenuItems.Remove(GameMenuModel.MenuId.continue_game);
            GameOver();
            GameMenu.MenuItems.Add(GameMenuModel.MenuId.continue_game, new GameMenuItem("Continue"));
            GameMenu.MenuItems[GameMenuModel.MenuId.continue_game].Action += Start;
        }

        private void RestartTheGame()
        {
            brd.Clear();
            snake.Reset();
            rabbit.Create();
            Step();
            Start();
        }
    }
}