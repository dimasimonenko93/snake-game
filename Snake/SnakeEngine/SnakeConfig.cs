﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace SnakeEngine
{
    public class SnakeConfig : ISnakeConfig
    {
       public int SpeedOfSnake { get; } = Convert.ToInt32(ConfigurationManager.AppSettings["SpeedOfSnake"]);
    }
}
