﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Timers;

namespace SnakeEngine
{
    public class Snake : ISnake
    {
        public enum Direction
        {
            Right,
            Left,
            Up,
            Down,
        }

        IBoard brd;
        IGameController controller;
        System.Timers.Timer timer;
        
        public event Action StepOnBody    = () => { }; // null object паттерн ?
        public event Action StepOnWall    = () => { }; // null object паттерн ?
        public event Action MovementDone  = () => { };

        public Direction SnakeDirection { get; private set; } =  Direction.Right;

        public Position HeadPosition { get; private set; }

        public List<Position> BodyPosition { get; } = new List<Position>();

        private Action move = () => { };


        public Snake(IBoard brd, IGameController controller, ISnakeConfig config)
        {
            this.brd        = brd;
            this.controller = controller;
            HeadPosition = new Position();
            Reset();
            controller.MoveDown  += () => move = MoveDown;
            controller.MoveUp    += () => move = MoveUp;
            controller.MoveRight += () => move = MoveRight;
            controller.MoveLeft  += () => move = MoveLeft;
            timer = new System.Timers.Timer(config.SpeedOfSnake);
            timer.Elapsed += Move;
            timer.AutoReset = true;
        }

        public void StartMove()
        {
            timer.Start();
        }

        public void StopMove()
        {
            lock (this)
            {
                timer.Stop();
            }
        }

        public void Reset()
        {
            BodyPosition.Clear();
            SnakeDirection = Direction.Right;
            HeadPosition.X = brd.Width / 2;
            HeadPosition.Y = brd.Height / 2;
            BodyPosition.Add(new Position(HeadPosition.X, HeadPosition.Y));
            brd[HeadPosition.Y, HeadPosition.X] = Board.CellStatus.SnakeHead;
            move = () => { };
        }

        private void Move(object sender, EventArgs e)
        {
            lock (this)
            {
                if (timer.Enabled)
                {
                    move();
                }
            }
        }

        private void MoveRight()
        {
            HeadPosition.X++;
            if (HeadPosition.X == brd.Right + 1)
            {
                HeadPosition.X = brd.Left;
            }
            SnakeDirection = Direction.Right;
            MoveBody();
            MovementDone();
        }

        private void MoveLeft()
        {
            HeadPosition.X--;
            if (HeadPosition.X == brd.Left - 1)
            {
                HeadPosition.X = brd.Right;
            }
            SnakeDirection = Direction.Left;
            MoveBody();
            MovementDone();
        }

        private void MoveUp()
        {
            HeadPosition.Y--;
            if (HeadPosition.Y == brd.Top - 1)
            {
                HeadPosition.Y = brd.Bottom;
            }
            SnakeDirection = Direction.Up;
            MoveBody();
            MovementDone();
        }

        private void MoveDown()
        {
            HeadPosition.Y++;
            if (HeadPosition.Y == brd.Bottom + 1)
            {
                HeadPosition.Y = brd.Top;
            }
            SnakeDirection = Direction.Down;
            MoveBody();
            MovementDone();
        }

        public void GrowBody()
        {
            BodyPosition.Add(new Position(HeadPosition.X, HeadPosition.Y));
        }

        private void MoveBody()
        {
            if (IsStepOnBody())
            {
                StopMove();
                StepOnBody();
            }
            else if (IsStepOnWall())
            {
                StopMove();
                StepOnWall();
            }
            else
            {
                brd[BodyPosition[0].Y, BodyPosition[0].X] = Board.CellStatus.Empty;
                BodyPosition.Add(new Position(HeadPosition.X, HeadPosition.Y));
                BodyPosition.RemoveAt(0);
                foreach (Position p in BodyPosition)
                {
                    brd[p.Y, p.X] = Board.CellStatus.SnakeBody;
                }
                brd[HeadPosition.Y, HeadPosition.X] = Board.CellStatus.SnakeHead;
            }
        }

        private bool IsStepOnBody() // using in MoveBody
        {
            foreach (Position p in BodyPosition)
            {
                if (HeadPosition.X == p.X && HeadPosition.Y == p.Y)
                {
                    return true;
                }
            }
            return false;
        }

        private bool IsStepOnWall() // using in MoveBody
        {
            if (brd[HeadPosition.Y, HeadPosition.X] == Board.CellStatus.Wall)
            {
                return true;
            }
            else return false;
        } 
    }
}