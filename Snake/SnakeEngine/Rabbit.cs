﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeEngine
{
    public class Rabbit : IRabbit
    {
        IBoard brd;

        public Position position { get; private set; }
        public int Count         { get; set; } = 0;

        private Random rX = new Random();
        private Random rY = new Random();

        public Rabbit(IBoard brd)
        {
            this.brd = brd;
            position = new Position();
            Create();
        }

        public void Create()
        {
            position.X = rX.Next(brd.Left, brd.Right);
            position.Y = rY.Next(brd.Top, brd.Bottom);
            brd[position.Y, position.X] = Board.CellStatus.Rabbit;
        }
    }
}
