﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeEngine
{
    public interface IRabbit
    {
        Position position { get; }
        int Count         { get; set; }
        void Create();
    }
}
