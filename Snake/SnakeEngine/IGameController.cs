﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace SnakeEngine
{
    public interface IGameController
    {
        event Action MoveDown;
        event Action MoveUp;
        event Action MoveRight;
        event Action MoveLeft;
        event Action ShowMenu;
        void Start();
        void Stop();
    }
}
