﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnakeEngine;

namespace ConsoleSnake
{
    public interface IConsoleMenuController
    {
        event Action MenuElementChanged;
        event Action MenuItemWasSelected;
        int ActiveMenuItem { get; }
        Position MenuPosition { get; }
    }
}
