﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SnakeEngine;

namespace ConsoleSnake
{
    public class ConsoleView
    {
        IBoard brd;
        IConsoleMenuController consoleMenuController;
        GameMenuModel gameMenu;

        public ConsoleView(IBoard brd, IGame game, IGameController consoleSnakeController, IConsoleMenuController consoleMenuController)
        {
            Console.CursorVisible = false;
            this.brd                   = brd;
            this.consoleMenuController = consoleMenuController;
            gameMenu                   = game.GameMenu;

            brd.Changed                                    += (p) => DisplayCell(p.Y, p.X);
            game.GameStarted                               += DisplayBoard;
            game.GameStoped                                += ShowGameOverMessage;
            this.consoleMenuController.MenuElementChanged  += OnMenuChanged;
            this.consoleMenuController.MenuItemWasSelected += Console.Clear;
            DisplayBoard();
        }

        private void OnMenuChanged()
        {
            Console.Clear();

            int i = 0;
            foreach (var item in gameMenu.MenuItems.Values)
            {
                Console.SetCursorPosition(consoleMenuController.MenuPosition.X, consoleMenuController.MenuPosition.Y + i * 2);

                Console.Write((consoleMenuController.ActiveMenuItem == i ? "=>" : "") + item.Text);

                i++;
            }
        }

        private void DisplayBoard()
        {
            Console.SetCursorPosition(0, 0);
            for (int i = 0; i < brd.Height; i++)
            {
                for (int j = 0; j < brd.Width; j++)
                {
                    DisplayCell(i, j);
                }
                Console.WriteLine();
            }
        }

        private void DisplayCell(int y, int x)
        {
            Console.SetCursorPosition(x, y);
            switch (brd[y, x])
            {
                case Board.CellStatus.Empty:
                    Console.Write(' ');
                    break;
                case Board.CellStatus.SnakeHead:
                    Console.Write('@');
                    break;
                case Board.CellStatus.SnakeBody:
                    Console.Write('o');
                    break;
                case Board.CellStatus.Rabbit:
                    Console.Write('&');
                    break;
                case Board.CellStatus.Wall:
                    Console.Write('*');
                    break;
            }
        }

        private void ShowGameOverMessage()
        {
            Console.Clear();
            Console.SetCursorPosition(consoleMenuController.MenuPosition.X, consoleMenuController.MenuPosition.Y);
            Console.Write("-- Game Over --");
            Console.SetCursorPosition(consoleMenuController.MenuPosition.X, consoleMenuController.MenuPosition.Y + 2);
            Console.Write("Press any key to continue...");
            Console.ReadKey(true);
        }
    }
}