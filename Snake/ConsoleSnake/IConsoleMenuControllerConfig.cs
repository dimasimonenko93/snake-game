﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSnake
{
    public interface IConsoleMenuControllerConfig : IConsoleControllerConfig
    {
        int MenuPositionX { get; set; }
        int MenuPositionY { get; set; }
    }
}
