﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using System.Configuration;
using SnakeEngine;

namespace ConsoleSnake
{
    public sealed class ConsoleGameController : GameController
    {
        System.Timers.Timer timer;

        private ConsoleKeyInfo Cki;
        private KeyCode CkiCode;

        public ConsoleGameController(IConsoleControllerConfig controllerConfig)
        {
            timer = new System.Timers.Timer(controllerConfig.ScanSpeed);
            timer.Elapsed += ReadKey;
            timer.AutoReset = true;
        }

        public override void Start()
        {
            timer.Start();
        }

        public override void Stop()
        {
            timer.Stop();
        }

        private void ReadKey(object sender, EventArgs e)
        {
            if (Console.KeyAvailable)
            {
                Cki = Console.ReadKey(true);

                if (Cki.Key == ConsoleKey.RightArrow)
                {
                    CkiCode = KeyCode.RightArrowCode;
                }
                else if (Cki.Key == ConsoleKey.LeftArrow)
                {
                    CkiCode = KeyCode.LeftArrowCode;
                }
                else if (Cki.Key == ConsoleKey.UpArrow)
                {
                    CkiCode = KeyCode.UpArrowCode;
                }
                else if (Cki.Key == ConsoleKey.DownArrow)
                {
                    CkiCode = KeyCode.DownArrowCode;
                }
                else if (Cki.Key == ConsoleKey.Escape)
                {
                    Stop();
                    CkiCode = KeyCode.EscapeCode;
                }
                else
                {
                    CkiCode = KeyCode.VoidCode;
                }

                ReadKey(CkiCode);
            }
        }
    }
}