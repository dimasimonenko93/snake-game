﻿using System.Configuration;
using SnakeEngine;

namespace ConsoleSnake
{
    public class ConsoleControllerConfig : IConsoleMenuControllerConfig
    {
        public int ScanSpeed     { get; set; } = System.Convert.ToInt32(ConfigurationManager.AppSettings["ScanConsoleControllerSpeed"]);
        public int MenuPositionX { get; set; } = System.Convert.ToInt32(ConfigurationManager.AppSettings["MenuPositionX"]);
        public int MenuPositionY { get; set; } = System.Convert.ToInt32(ConfigurationManager.AppSettings["MenuPositionY"]);
    }
}