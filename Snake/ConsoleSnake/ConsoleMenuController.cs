﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnakeEngine;

namespace ConsoleSnake
{
    class ConsoleMenuController : IConsoleMenuController
    {
        GameMenuModel gameMenu;

        public event Action MenuElementChanged;

        public event Action MenuItemWasSelected;

        public Position MenuPosition { get; }

        public int ActiveMenuItem { get; private set; } = 0;

        public ConsoleMenuController(IConsoleMenuControllerConfig controllerConfig, IGameController consoleSnakeController, IGame game)
        {
            gameMenu = game.GameMenu;

            MenuPosition = new Position(controllerConfig.MenuPositionX, controllerConfig.MenuPositionY);

            game.GamePaused += StartMenu;
            game.GameOver   += StartMenu;
        }

        private void StartMenu()
        {
            int MenuItemsCount = gameMenu.MenuItems.Count;

            for(; ; )
            {
                MenuElementChanged();

                ConsoleKeyInfo Cki = Console.ReadKey(true);

                if (Cki.Key == ConsoleKey.Enter)
                {
                    MenuItemWasSelected();

                    var MenuItems = gameMenu.MenuItems.Values.ToList();

                    MenuItems[ActiveMenuItem].CallEvent();
                    ActiveMenuItem = 0;
                    break;
                }
                else if (Cki.Key == ConsoleKey.DownArrow)
                {
                    ActiveMenuItem++;

                    if (ActiveMenuItem > MenuItemsCount - 1)
                    {
                        ActiveMenuItem = 0;
                    }
                }
                else if (Cki.Key == ConsoleKey.UpArrow)
                {
                    ActiveMenuItem--;

                    if (ActiveMenuItem < 0)
                    {
                        ActiveMenuItem = MenuItemsCount - 1;
                    }
                }
            }
        }
    }
}
