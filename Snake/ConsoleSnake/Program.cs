﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using SnakeEngine;

namespace ConsoleSnake
{

    class Program
    {
        private static AutoResetEvent gameOver = new AutoResetEvent(false);

        static void Main(string[] args)
        {
            IBoardConfig boardConfig = new BoardConfig();
            ISnakeConfig snakeConfig = new SnakeConfig();
            IConsoleMenuControllerConfig controllerConfig = new ConsoleControllerConfig();

            IBoard brd = new Board(boardConfig);
            IGameController consoleGameController = new ConsoleGameController(controllerConfig);
            ISnake snake = new Snake(brd, consoleGameController, snakeConfig);
            IRabbit rabbit = new Rabbit(brd);

            IGame game = new Game(brd, snake, rabbit, consoleGameController);
            IConsoleMenuController consoleMenuController = new ConsoleMenuController(controllerConfig, consoleGameController, game);
            ConsoleView view = new ConsoleView(brd, game, consoleGameController, consoleMenuController);

            game.GameMenu.MenuItems[GameMenuModel.MenuId.exit_game].Action += () => gameOver.Set();

            game.Start();

            gameOver.WaitOne();
        }
    }
}