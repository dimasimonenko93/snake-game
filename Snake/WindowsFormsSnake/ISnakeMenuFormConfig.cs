﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsSnake
{
    public interface ISnakeMenuFormConfig
    {
        int Width { get; }
        int Height { get; }
        int MinimumWidth { get; }
        int MinimumHeight { get; }
        int MaximumWidth { get; }
        int MaximumHeight { get; }
        int DistanceBetweenButtons { get; }
    }
}
