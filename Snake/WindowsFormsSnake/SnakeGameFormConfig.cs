﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsSnake
{
    class SnakeGameFormConfig : ISnakeGameFormConfig
    {
        public int ElementSize { get; } = Convert.ToInt32(ConfigurationManager.AppSettings["SnakeGameFormElementSize"]);
    }
}
