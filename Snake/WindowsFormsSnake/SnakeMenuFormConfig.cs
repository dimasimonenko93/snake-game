﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsSnake
{
    public class SnakeMenuFormConfig : ISnakeMenuFormConfig
    {
        public int Width { get; } = Convert.ToInt32(ConfigurationManager.AppSettings["SnakeMenuFormWidth"]);

        public int Height { get; } = Convert.ToInt32(ConfigurationManager.AppSettings["SnakeMenuFormHeight"]);

        public int MinimumWidth { get; } = Convert.ToInt32(ConfigurationManager.AppSettings["SnakeMenuFormMinimumWidth"]);

        public int MinimumHeight { get; } = Convert.ToInt32(ConfigurationManager.AppSettings["SnakeMenuFormMinimumHeight"]);

        public int MaximumWidth { get; } = Convert.ToInt32(ConfigurationManager.AppSettings["SnakeMenuFormMaximumWidth"]);

        public int MaximumHeight { get; } = Convert.ToInt32(ConfigurationManager.AppSettings["SnakeMenuFormMaximumHeight"]);

        public int DistanceBetweenButtons { get; } = Convert.ToInt32(ConfigurationManager.AppSettings["SnakeMenuFormDistanceBetweenButtons"]);
    }
}
