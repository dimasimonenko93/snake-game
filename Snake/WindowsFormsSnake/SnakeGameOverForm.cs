﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SnakeEngine;

namespace WindowsFormsSnake
{
    public partial class SnakeGameOverForm : Form
    {
        public SnakeGameOverForm(GameMenuModel gameMenu, ISnakeMenuFormConfig formConfig)
        {
            InitializeComponent();

            Size = new Size(formConfig.Width, formConfig.Height);

            int newButtonYLocation = buttonTemplate.Location.Y;

            foreach (var item in gameMenu.MenuItems.Values)
            {
                Button b = new Button();
                b.Name = item.Text;
                b.Text = item.Text;
                b.Size = new Size(buttonTemplate.Size.Width, buttonTemplate.Size.Height);
                b.TabIndex = buttonTemplate.TabIndex++;
                b.Location = new Point(buttonTemplate.Location.X, newButtonYLocation);
                b.Click += (object sender, EventArgs e) => { Hide(); item.CallEvent(); };
                b.Visible = true;
                Controls.Add(b);
                newButtonYLocation = b.Location.Y + formConfig.DistanceBetweenButtons;
            }
        }
    }
}
