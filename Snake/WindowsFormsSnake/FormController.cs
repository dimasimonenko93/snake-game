﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SnakeEngine;

namespace WindowsFormsSnake
{
    public sealed class FormController : GameController
    {
        private Keys Cki;
        private KeyCode CkiCode;

        public void ReadKey(object sender, KeyEventArgs e)
        {
            Cki = e.KeyCode;

            if (Cki == Keys.Right)
            {
                CkiCode = KeyCode.RightArrowCode;
            }
            else if (Cki == Keys.Left)
            {
                CkiCode = KeyCode.LeftArrowCode;
            }
            else if (Cki == Keys.Up)
            {
                CkiCode = KeyCode.UpArrowCode;
            }
            else if (Cki == Keys.Down)
            {
                CkiCode = KeyCode.DownArrowCode;
            }
            else if (Cki == Keys.Escape)
            {
                CkiCode = KeyCode.EscapeCode;
            }
            else
            {
                CkiCode = KeyCode.VoidCode;
            }

            ReadKey(CkiCode);
        }
    }
}
