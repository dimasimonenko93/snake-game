﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using SnakeEngine;

namespace WindowsFormsSnake
{
    public partial class SnakeGameForm : Form
    {
        IBoard               brd;
        FormController       gameController;
        IGame                game;
        SnakeMenuForm        menuForm;
        ISnake               snake;
        ISnakeMenuFormConfig menuConfig;

        public int ElementSize;

        private Image SnakeHeadImage;
        private Image SnakeBodyImage;
        private Image RabbitImage;
        private Image WallImage;

        public SnakeGameForm()
        {
            InitializeComponent();

            SnakeHeadImage = Properties.Resources.SnakeHead;
            SnakeBodyImage = Properties.Resources.SnakeBody;
            RabbitImage    = Properties.Resources.Rabbit;
            WallImage      = Properties.Resources.Wall;

            ISnakeGameFormConfig snakeGameFormConfig = new SnakeGameFormConfig();
            IBoardConfig boardConfig                 = new BoardConfig();
            ISnakeConfig snakeConfig                 = new SnakeConfig();
            menuConfig                               = new SnakeMenuFormConfig();


            brd = new Board(boardConfig);
            gameController = new FormController();
            snake          = new Snake(brd, gameController, snakeConfig);

            IRabbit rabbit = new Rabbit(brd);

            game         = new Game(brd, snake, rabbit, gameController);
            menuForm     = new SnakeMenuForm(game.GameMenu, menuConfig);

            ElementSize = snakeGameFormConfig.ElementSize;
            ClientSize  = new Size(ElementSize * brd.Width, ElementSize * brd.Height);

            brd.Changed             += (p) => Invalidate();
            gameController.ShowMenu += () => menuForm.ShowDialog(this);
            game.GameOver           += ShowEndGameMenu;

            game.GameMenu.MenuItems[GameMenuModel.MenuId.exit_game].Action += Close;

            Invalidate();
            game.Start();
        }

        private void FormView_KeyDown(object sender, KeyEventArgs e)
        {
            gameController.ReadKey(sender, e);
        }

        private void FormView_Paint(object sender, PaintEventArgs e)
        {
            for (int i = 0; i < brd.Height; i++)
            {
                for (int j = 0; j < brd.Width; j++)
                {
                    switch (brd[i, j])
                    {
                        case Board.CellStatus.SnakeHead:
                            RotateAndDrawSnakeHead(i, j, e);
                            break;
                        case Board.CellStatus.SnakeBody:
                            e.Graphics.DrawImage(SnakeBodyImage, j * ElementSize, i * ElementSize, ElementSize, ElementSize);
                            break;
                        case Board.CellStatus.Rabbit:
                            e.Graphics.DrawImage(RabbitImage, j * ElementSize, i * ElementSize, ElementSize, ElementSize);
                            break;
                        case Board.CellStatus.Wall:
                            e.Graphics.DrawImage(WallImage, j * ElementSize, i * ElementSize, ElementSize, ElementSize);
                            break;
                    }
                }
            }
        }

        private void RotateAndDrawSnakeHead(int i, int j, PaintEventArgs e)
        {
            Snake.Direction Direction = snake.SnakeDirection;

            if (Direction == Snake.Direction.Right)
            {
                e.Graphics.DrawImage(SnakeHeadImage, j * ElementSize, i * ElementSize, ElementSize, ElementSize);
            }
            else if (Direction == Snake.Direction.Left)
            {
                SnakeHeadImage.RotateFlip(RotateFlipType.Rotate180FlipNone);
                e.Graphics.DrawImage(SnakeHeadImage, j * ElementSize, i * ElementSize, ElementSize, ElementSize);
                SnakeHeadImage.RotateFlip(RotateFlipType.Rotate180FlipNone);
            }
            else if (Direction == Snake.Direction.Up)
            {
                SnakeHeadImage.RotateFlip(RotateFlipType.Rotate270FlipNone);
                e.Graphics.DrawImage(SnakeHeadImage, j * ElementSize, i * ElementSize, ElementSize, ElementSize);
                SnakeHeadImage.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }
            else if (Direction == Snake.Direction.Down)
            {
                SnakeHeadImage.RotateFlip(RotateFlipType.Rotate90FlipNone);
                e.Graphics.DrawImage(SnakeHeadImage, j * ElementSize, i * ElementSize, ElementSize, ElementSize);
                SnakeHeadImage.RotateFlip(RotateFlipType.Rotate270FlipNone);
            }
            else
            {
                e.Graphics.DrawImage(SnakeHeadImage, j * ElementSize, i * ElementSize, ElementSize, ElementSize);
            }
        }

        private void ShowEndGameMenu()
        {
            var endMenu  = new SnakeGameOverForm(game.GameMenu, menuConfig);

            BeginInvoke((Action)(() => endMenu.ShowDialog(this)));
        }
    }
}