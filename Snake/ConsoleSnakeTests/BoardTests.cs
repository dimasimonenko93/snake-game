﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleSnake;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSnake.Tests
{
    [TestClass()]
    public class BoardTests
    {
        [TestMethod()]
        public void CreateWall_10x7x1()
        {
            Config config = new Config
            {
                Width = 10,
                Height = 7,
                Frame = 1
            };

            Board brd = new Board(config);

            Assert.AreEqual(CellStatus.Wall, brd[0, 0]);
            Assert.AreEqual(CellStatus.Wall, brd[0, 9]);
            Assert.AreEqual(CellStatus.Wall, brd[6, 0]);
            Assert.AreEqual(CellStatus.Wall, brd[6, 9]);
        }
    }
    
    [TestClass()]
    public class BoardTests2
    {
        [TestMethod()]
        public void CreateWall_11x16x2()
        {
            Config config = new Config
            {
                Width = 11,
                Height = 16,
                Frame = 2
            };

            Board brd = new Board(config);

            Assert.AreEqual(CellStatus.Wall, brd[0, 0]);
            Assert.AreEqual(CellStatus.Wall, brd[1, 1]);
            Assert.AreEqual(CellStatus.Wall, brd[1, 9]);
            Assert.AreEqual(CellStatus.Wall, brd[14, 1]);
            Assert.AreEqual(CellStatus.Wall, brd[14, 9]);
        }
    }
}